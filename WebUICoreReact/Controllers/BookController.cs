﻿using System.Collections.Generic;
using System.Linq;
using BaseCore.Entities;
using BaseCore.Services.Abstract;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebUICoreReact.Controllers
{
    [Route("api/books")]
    public class BookController : Controller
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        // GET: api/values
        [HttpGet]
        [Route("getall")]
        public JsonResult Get(int page, int count)
        {
            int total = _bookService.GetAll().Count;

            int skip = (page - 1) * count;

            var data = _bookService.GetAll().Skip(skip).Take(count);

            return new JsonResult(new
            {
                Total = total,
                Data = data
            });            
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
