﻿using Microsoft.AspNetCore.Mvc;

namespace WebUICoreReact.Controllers
{
    public class HomeController : Controller
    {
        // GET
        public IActionResult Index()
        {
            return View();
        }
    }
}