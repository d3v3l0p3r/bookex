﻿"use strict";

let path = require('path');

const CleanWebpackPlugin = require('clean-webpack-plugin');

const bundleFolder  = "wwwroot/bundle";

module.exports = {
    entry: {
        app: "./ClientApp/app.tsx"  // Точка входа           
    },
    output: {                       
        filename: "./dist/bundle.js",   // Выходной файл
        path: path.resolve(__dirname, bundleFolder),
        publicPath: path.resolve(__dirname, bundleFolder)
    },
    devtool: "source-map",
    devServer: {
        contentBase: ".",
        host: "localhost",
        port: 9000
    },
    plugins : [
        new CleanWebpackPlugin([bundleFolder])        
    ],    
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader"
            },
            {
                test: /\.tsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: "ts-loader"
            }       
        ]
    }
};