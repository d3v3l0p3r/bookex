﻿import * as React from "react"
import * as $ from "jquery"
import { BookListView } from "./BookListView"
import { Book } from "./Entities/book"

export interface IBookListState {
    page: number,
    pageCount: number,
    total: number,
    books: Array<Book>,
}

export interface IBookListProps {
    getUrl: string;
}

export class BookList extends React.Component<IBookListProps, IBookListState> {

    state: IBookListState = { page : 0, pageCount: 10, total : 0, books : [] };

    constructor(props: IBookListProps) {
        super(props);

        this.nextPage = this.nextPage.bind(this);
        this.prevPage = this.prevPage.bind(this);       
    }

    render() {        
        return <div>

            <table className="" >
                <thead>
                    <tr>
                        <th>Title </th>
                        <th> PageCount </th>
                        <th> Publisher </th>
                        <th> PublishDate </th>
                        <th> ISBN </th>
                    </tr>
                </thead>
                <tbody>                                    
                    {                       
                        this.state.books.map(book => <BookListView book={book} key={book.id}/>)
                    }
                </tbody>
            </table>
            <div>
                <button onClick={this.prevPage}>prev</button>
                <button onClick={this.nextPage}>next</button>
                <span> {this.state.page} : {this.state.total} </span>
            </div>
        </div>;
    }

    nextPage() {
        this.setState({ page: this.state.page + 1 });
        this.loadData();
    }

    prevPage() {

        if (this.state.page > 0) {
            this.setState({ page: this.state.page - 1 });
            this.loadData();
        }
    }

    loadData() {        
        var data = {
            page: this.state.page,
            count: this.state.pageCount
        };

        $.ajax({
            url: this.props.getUrl,
            type: 'get',
            data: data,
            success: result => {                
                this.setState({ total: result.total });
                this.setState({ books: result.data });
            }
        });
    }
    
    componentDidMount(): void {        
        this.loadData();
    }
}