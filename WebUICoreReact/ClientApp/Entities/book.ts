﻿export class Book {
    id: string;
    title: string;
    pageCount: number;
    publisher: string;
    publishDate: Date;
    isbn: string;
}