﻿import {Book} from "./Entities/book"
import * as React from "react";

interface IBookListViewProps {
    book : Book
}

interface IBookListViewState {
    
}

export class BookListView extends React.Component<IBookListViewProps, Book> {
    constructor(props) {
        super(props);
        this.state =  props.book;
        this.open = this.open.bind(this);
    }

    render() {
        return <tr className="k-panel">
                   <td><p>{this.state.title}</p></td>
                   <td><p>{this.state.pageCount}</p></td>
                   <td><p>{this.state.publisher}</p></td>
                   <td><p>{this.state.publishDate}</p></td>
                   <td><p>{this.state.isbn}</p></td>
               </tr>;
    }

    open() {

    }
}