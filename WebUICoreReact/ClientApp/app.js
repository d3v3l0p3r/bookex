"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var BookList_1 = require("./BookList");
ReactDOM.render(React.createElement(BookList_1.BookList, { getUrl: 'api/books/getall' }), document.getElementById("content"));
//# sourceMappingURL=app.js.map