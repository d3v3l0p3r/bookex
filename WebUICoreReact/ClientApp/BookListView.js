"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var BookListView = (function (_super) {
    __extends(BookListView, _super);
    function BookListView(props) {
        var _this = _super.call(this, props) || this;
        _this.state = props.book;
        _this.open = _this.open.bind(_this);
        return _this;
    }
    BookListView.prototype.render = function () {
        return React.createElement("tr", { className: "k-panel" },
            React.createElement("td", null,
                React.createElement("p", null, this.state.title)),
            React.createElement("td", null,
                React.createElement("p", null, this.state.pageCount)),
            React.createElement("td", null,
                React.createElement("p", null, this.state.publisher)),
            React.createElement("td", null,
                React.createElement("p", null, this.state.publishDate)),
            React.createElement("td", null,
                React.createElement("p", null, this.state.isbn)));
    };
    BookListView.prototype.open = function () {
    };
    return BookListView;
}(React.Component));
exports.BookListView = BookListView;
//# sourceMappingURL=BookListView.js.map