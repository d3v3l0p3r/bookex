﻿import * as React from "react"
import * as ReactDOM from "react-dom"
import {BookList} from "./BookList"

ReactDOM.render(<BookList getUrl='api/books/getall' />, document.getElementById("content"));