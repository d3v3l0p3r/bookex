"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var BookDetailView = (function (_super) {
    __extends(BookDetailView, _super);
    function BookDetailView() {
        return _super.call(this) || this;
    }
    BookDetailView.prototype.render = function () {
        return React.createElement("div", null,
            React.createElement("label", null, "Title"),
            React.createElement("input", { type: "text", value: "{this.state.data.Title}" }));
    };
    return BookDetailView;
}(React.Component));
exports.BookDetailView = BookDetailView;
//# sourceMappingURL=BookDetailView.js.map