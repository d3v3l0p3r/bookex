"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var $ = require("jquery");
var BookListView_1 = require("./BookListView");
var BookList = (function (_super) {
    __extends(BookList, _super);
    function BookList(props) {
        var _this = _super.call(this, props) || this;
        _this.state = { page: 0, pageCount: 10, total: 0, books: [] };
        _this.nextPage = _this.nextPage.bind(_this);
        _this.prevPage = _this.prevPage.bind(_this);
        return _this;
    }
    BookList.prototype.render = function () {
        return React.createElement("div", null,
            React.createElement("table", { className: "" },
                React.createElement("thead", null,
                    React.createElement("tr", null,
                        React.createElement("th", null, "Title "),
                        React.createElement("th", null, " PageCount "),
                        React.createElement("th", null, " Publisher "),
                        React.createElement("th", null, " PublishDate "),
                        React.createElement("th", null, " ISBN "))),
                React.createElement("tbody", null, this.state.books.map(function (book) { return React.createElement(BookListView_1.BookListView, { book: book, key: book.id }); }))),
            React.createElement("div", null,
                React.createElement("button", { onClick: this.prevPage }, "prev"),
                React.createElement("button", { onClick: this.nextPage }, "next"),
                React.createElement("span", null,
                    " ",
                    this.state.page,
                    " : ",
                    this.state.total,
                    " ")));
    };
    BookList.prototype.nextPage = function () {
        this.setState({ page: this.state.page + 1 });
        this.loadData();
    };
    BookList.prototype.prevPage = function () {
        if (this.state.page > 0) {
            this.setState({ page: this.state.page - 1 });
            this.loadData();
        }
    };
    BookList.prototype.loadData = function () {
        var _this = this;
        var data = {
            page: this.state.page,
            count: this.state.pageCount
        };
        $.ajax({
            url: this.props.getUrl,
            type: 'get',
            data: data,
            success: function (result) {
                _this.setState({ total: result.total });
                _this.setState({ books: result.data });
            }
        });
    };
    BookList.prototype.componentDidMount = function () {
        this.loadData();
    };
    return BookList;
}(React.Component));
exports.BookList = BookList;
//# sourceMappingURL=BookList.js.map