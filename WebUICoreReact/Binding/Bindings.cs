﻿using BaseCore.DAL;
using BaseCore.Entities;
using BaseCore.Services.Abstract;
using BaseCore.Services.Concrete;
using SimpleInjector;

namespace WebUICoreReact.Binding
{
    public class Bindings
    {

        private static Container _container;

        private static Container GetContainer()
        {
            if(_container == null)
                _container = new Container();

            return _container;
        }

        
        public static Container Init()
        {
            var container = GetContainer();

            container.Register<IRepository<Book>, BookRepository>();
            container.Register<IBookService, BookService>();

            return container;
        }
    }
}
