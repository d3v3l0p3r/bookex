﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Integration.WebApi;

namespace WebUIReact
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            {
                // Код, выполняемый при запуске приложения
                AreaRegistration.RegisterAllAreas();
                GlobalConfiguration.Configure(WebApiConfig.Register);
                RouteConfig.RegisterRoutes(RouteTable.Routes);



                var container = Bindings.GetContainer();

                Bindings.Init(container);

                container.Verify();

                DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
                GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
            }
        }
    }
}
