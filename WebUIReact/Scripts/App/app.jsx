﻿class Book extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: props.book };
        this.open = this.open.bind(this);
    }

    render() {
        return <tr className="k-panel" onDblClick={this.open}>
            <td><p>{this.state.data.Title}</p></td>
            <td><p>{this.state.data.PageCount}</p></td>
            <td><p>{this.state.data.Publisher}</p></td>
            <td><p>{this.state.data.PublishDate}</p></td>
            <td><p>{this.state.data.ISBN}</p></td>
        </tr>;
    }

    open() {
        
    }
}


class BookList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            books: [],
            page: 1,
            pageCount: 10,
            total: 0
        };

        this.nextPage = this.nextPage.bind(this);
        this.prevPage = this.prevPage.bind(this);


    }

    render() {
        return <div>

            <table className="kendo-grid">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>PageCount</th>
                        <th>Publisher</th>
                        <th>PublishDate</th>
                        <th>ISBN</th>
                    </tr>
                </thead>
                <tbody>
                    {

                        this.state.books.map(function (book) {
                            return <Book key={book.ID} book={book} />;
                        })
                    }
                </tbody>
            </table>

            <div>
                <button onClick={this.prevPage}>prev</button>
                <button onClick={this.nextPage}>next</button>
                <span>{this.state.page} : {this.state.total}</span>
            </div>
        </div>;
    }

    nextPage() {
        this.state.page++;
        this.loadData();
    }

    prevPage() {

        if (this.state.page > 0) {
            this.state.page--;
            this.loadData();
        }
    }

    loadData() {

        var self = this;
        var data = {
            page: this.state.page,
            count: this.state.pageCount
        };

        $.ajax({
            url: this.props.getUrl,
            type: 'get',
            data: data,
            success: function (data) {
                self.setState({ total: data.Total });
                self.setState({ books: data.Data });
            }
        });
    }



    componentDidMount() {
        this.loadData();
    }
}

//ReactDOM.render(element, document.getElementById('content'));

ReactDOM.render(<BookList getUrl='api/books/getall' />, document.getElementById("content"));