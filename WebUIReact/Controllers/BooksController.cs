﻿using System;
using System.Linq;
using System.Web.Http;
using BaseCore.Entities;
using BaseCore.Services.Abstract;

namespace WebUIReact.Controllers
{
    [RoutePrefix("api/books")]
    public class BooksController : ApiController
    {
        private readonly IBookService _bookService;

        public BooksController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpGet]
        [Route("getall")]
        public IHttpActionResult GetAll(int page, int count)
        {
            int skip = --page * count;

            var all = _bookService.GetAll();

            int total = all.Count;

            all = all.Skip(skip).Take(count).ToList();

            return Ok(new
            {
                Data = all,
                Total = total
            });
        }

        public IHttpActionResult Get(Guid id)
        {
            var book = _bookService.Get(id);
            return Ok(book);
        }

        [HttpPost]
        public IHttpActionResult Post(Book book)
        {
            try
            {
                var res = _bookService.Update(book);
                return Ok(res);
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                _bookService.Delete(id);
                return Ok(new { res = "ok" });
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
        }
    }
}
