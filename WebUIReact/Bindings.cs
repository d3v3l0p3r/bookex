﻿
using BaseCore.DAL;
using BaseCore.Entities;
using BaseCore.Services.Abstract;
using BaseCore.Services.Concrete;
using SimpleInjector;

namespace WebUIReact
{
    public class Bindings
    {
        private static Container container;

        public static Container GetContainer()
        {
            if (container == null)
            {
                container = new Container();            
                
            }            
            return container;
        }

        public static void Init(Container container)
        {
            container.Register<IRepository<Book>, BookRepository>(Lifestyle.Singleton);
            container.Register<IBookService, BookService>();
            //container.Register<IFileSystemService, FileSystemService>();
        }


    }
}