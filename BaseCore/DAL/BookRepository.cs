using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using BaseCore.Entities;

namespace BaseCore.DAL
{
    public class BookRepository : IRepository<Book>
    {
        private readonly ConcurrentDictionary<Guid, Book> _books;

        public BookRepository()
        {
            _books = new ConcurrentDictionary<Guid, Book>();
            var random = new Random();
            for (int i = 0; i < 1000; i++)
            {
                var id = Guid.NewGuid();
                var book = new Book
                {
                    ID = id,
                    Authors = new List<Author>() {
                        new Author
                        {
                            FirstName = Faker.Name.First(),
                            LastName = Faker.Name.Last()
                        }
                    },
                    PageCount = random.Next(1, 9999),
                    Title = Faker.Company.Name(),
                    Publisher = Faker.Company.Name(),
                    PublishDate = DateTime.Now
                };
                _books.TryAdd(id, book);

            }
        }

        public Book Create(Book book)
        {
            book.ID = Guid.NewGuid();
            _books.TryAdd(book.ID, book);

            return book;
        }

        public void Delete(Guid id)
        {
            Book res;
            _books.TryRemove(id, out res);
        }

        public Book Get(Guid id)
        {
            Book book;

            _books.TryGetValue(id, out book);

            if (book == null)
            {
                throw new KeyNotFoundException();
            }

            return book;
        }

        public ICollection<Book> GetAll()
        {
            return _books.Values;
        }

        public Book Update(Book book)
        {
            Book old = Get(book.ID);

            _books.TryUpdate(book.ID, book, old);

            return book;
        }
    }
}