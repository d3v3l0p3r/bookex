﻿using System;
using System.Collections.Generic;
using BaseCore.Entities;

namespace BaseCore.DAL
{
    public interface IRepository<T> where T: IEntity
    {
        T Create(T book);
        void Delete(Guid id);
        T Get(Guid id);
        ICollection<T> GetAll();
        T Update(T book);
    }
}
