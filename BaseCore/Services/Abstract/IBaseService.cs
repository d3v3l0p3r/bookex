﻿using System;
using System.Collections.Generic;
using BaseCore.Entities;

namespace BaseCore.Services.Abstract
{
    public interface IBaseService<T> where T : IEntity
    {
        T Create(T book);
        void Delete(Guid id);
        T Get(Guid id);
        ICollection<T> GetAll();
        T Update(T book);
    }
}
