﻿using BaseCore.Entities;

namespace BaseCore.Services.Abstract
{
    public interface IBookService : IBaseService<Book>
    {
        
    }
}
