﻿using System;
using System.Linq;
using BaseCore.DAL;
using BaseCore.Entities;
using BaseCore.Services.Abstract;

namespace BaseCore.Services.Concrete
{
    public class BookService : BaseService<Book>, IBookService
    {
        public BookService(IRepository<Book> repository) : base(repository)
        {
        }

        public override Book Update(Book book)
        {
            Validate(book);

            return base.Update(book);
        }

        private void Validate(Book book)
        {
            if (book == null)
                throw new ArgumentNullException(nameof(book));

            if (string.IsNullOrEmpty(book.Title))
            {
                throw new Exception("Название не может быть пустым");
            }

            if (book.Title.Length > 30)
            {
                throw new Exception("Название не может больше 30 символов");
            }

            if (book.Authors == null || !book.Authors.Any())
            {
                throw new Exception("Книга должна содержать хотя бы одного автора");
            }

            if (book.Publisher != null && book.Publisher.Length > 30)
            {
                throw new Exception("Издательство не больше 30 символов");
            }

            if (book.PublishDate.Year < 1800)
            {
                throw new Exception("Издательство не больше 30 символов");
            }

            if (string.IsNullOrEmpty(book.ISBN))
            {
                throw new Exception("ISBN not nullable");
            }
            else
            {
                if (!CheckIsbn(book.ISBN))
                    throw new Exception("ISBN not valid");
            }            
        }

        private bool CheckIsbn(string isbn) // string must have 9 digits
        {
            if (isbn == null)
                return false;

            isbn = isbn.Normalize().Replace("-","").Replace(" ", "");

            if (isbn.Length != 10)
                return false;

            int result;
            for (int i = 0; i < 9; i++)
                if (!int.TryParse(isbn[i].ToString(), out result))
                    return false;

            int sum = 0;
            for (int i = 0; i < 9; i++)
                sum += (i + 1) * int.Parse(isbn[i].ToString());

            int remainder = sum % 11;
            if (remainder == 10)
                return isbn[9] == 'X';
            else
                return isbn[9] == (char)('0' + remainder);
        }
    }
}
