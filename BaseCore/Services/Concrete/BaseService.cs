﻿using System;
using System.Collections.Generic;
using BaseCore.DAL;
using BaseCore.Entities;
using BaseCore.Services.Abstract;

namespace BaseCore.Services.Concrete
{
    public class BaseService<T> : IBaseService<T> where T : IEntity
    {

        private readonly IRepository<T> _repository;

        public BaseService(IRepository<T> repository)
        {
            _repository = repository;
        }

        public virtual T Create(T book)
        {
            return _repository.Create(book);
        }

        public virtual void Delete(Guid id)
        {
            _repository.Delete(id);
        }

        public virtual T Get(Guid id)
        {
            return _repository.Get(id);
        }

        public virtual ICollection<T> GetAll()
        {
            return _repository.GetAll();
        }

        public virtual T Update(T book)
        {
            if (book.ID == Guid.Empty)
            {
                return _repository.Create(book);
            }

            return _repository.Update(book);
        }
    }
}
