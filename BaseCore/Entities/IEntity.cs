﻿using System;

namespace BaseCore.Entities
{
    public interface IEntity
    {
        Guid ID { get; set; }
    }
}