﻿using System;
using System.Collections.Generic;

namespace BaseCore.Entities
{
    public class Book : IEntity
    {
        public Guid ID { get; set; }
        public string Title { get; set; }
        public int PageCount { get; set; }
        public string Publisher { get; set; }
        public DateTime PublishDate { get; set; }
        public string ISBN { get; set; }
        public ICollection<Author> Authors { get; set; } = new List<Author>();
        public FileData File { get; set; }
    }
}
