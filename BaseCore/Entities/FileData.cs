﻿using System;

namespace BaseCore.Entities
{
    public class FileData
    {
        public Guid FileID { get; set; }
        public string FileName { get; set; }
    }
}
