﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace WebUI.App_Start
{
    public class BoundleConfig
    {
        private const string StyleNamespace = "CSS";
        private const string ScriptNamespace = "JS";

        public static void RegisterBoundles(BundleCollection boundles)
        {
            RegisterStyle(boundles, "Kendo", new []
            {
                "~/Content/vendor/bootstrap/bootstrap.css",
                "~/Content/vendor/kendo/styles/kendo.common.min.css",
                "~/Content/vendor/kendo/styles/kendo.common-material.min.css",
                "~/Content/vendor/kendo/styles/kendo.material.min.css",
                "~/Content/vendor/kendo/styles/kendo.mobile.switch.css",
            });

            RegisterScriptBundle(boundles, "Common", new []
            {
                "~/Scripts/vendor/jszip/jszip.js",
                "~/Scripts/vendor/kendo/kendo.all.js",                
                "~/Scripts/vendor/jquery/jquery-2.1.4.js",
            });
        }

        private static void RegisterStyle(BundleCollection bundleCollection, string bundleName, string[] files)
        {
            var bundle = new StyleBundle($"~/{StyleNamespace}/{bundleName}");

            foreach (var file in files)
            {
                bundle.Include(file);
            }

            bundleCollection.Add(bundle);
        }

        private static void RegisterScriptBundle(BundleCollection bundleCollection, string bundleName, string[] files)
        {
            var bundle = new ScriptBundle($"~/{ScriptNamespace}/{bundleName}");

            bundle.Include(files);

            bundleCollection.Add(bundle);
        }
    }
}