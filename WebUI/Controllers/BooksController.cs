﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebUI.Models;
using System.Linq.Dynamic;
using BaseCore.Entities;
using BaseCore.Services.Abstract;

namespace WebUI.Controllers
{
    [RoutePrefix("api/books")]
    public class BooksController : ApiController
    {
        private readonly IBookService _bookService;

        public BooksController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpPost]
        [Route("getall")]
        public IHttpActionResult GetAll(KendoRequestModel request)
        {
            var all = _bookService.GetAll();
            var total = all.Count;

            if (request.sort != null && request.sort.Count > 0)
            {
                var sorts = new List<string>();
                request.sort.ForEach(x => {
                    sorts.Add($"{x.field} {x.dir}");
                });

                string order = string.Join(",", sorts.ToArray());

                all = all.OrderBy(order).ToList();
            }

            var res = all.Skip(request.skip).Take(request.take);

            return Ok(new { Total = total, Data = res });
        }

        public IHttpActionResult Get(Guid id)
        {
            var book = _bookService.Get(id);
            return Ok(book);
        }

        [HttpPost]
        public IHttpActionResult Post(Book book)
        {
            try
            {
                var res = _bookService.Update(book);
                return Ok(res);
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                _bookService.Delete(id);
                return Ok(new { res = "ok" });
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
        }
    }
}
