﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebUI.Concrete;


namespace WebUI.Controllers
{
    public class FileController : ApiController
    {
        private readonly IFileSystemService _fileSystemService;
        public FileController(IFileSystemService fileSystemService)
        {
            _fileSystemService = fileSystemService;
        }

        [HttpPost]
        public IHttpActionResult Upload()
        {
            var request = HttpContext.Current.Request;

            if (request.Files.Count > 0)
            {
                var file = request.Files[0];

                var fileData = _fileSystemService.SaveFile(file);

                return Ok(fileData);
            }

            return BadRequest();
        }


        public HttpResponseMessage GetFile(Guid fileID)
        {
            string path = _fileSystemService.GetFilePath(fileID);

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(File.OpenRead(path))
            };

            return response;
        }

    }
}
