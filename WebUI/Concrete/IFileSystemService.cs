﻿using System;
using System.Web;
using BaseCore.Entities;

namespace WebUI.Concrete
{
    public interface IFileSystemService
    {
        string GetFilePath(Guid fileid);
        FileData SaveFile(HttpPostedFile postedFile);
    }
}