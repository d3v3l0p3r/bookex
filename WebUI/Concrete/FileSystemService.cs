﻿using System;
using System.IO;
using System.Web;
using BaseCore.Entities;

namespace WebUI.Concrete
{
    public class FileSystemService : IFileSystemService
    {
        private readonly string _filesDirectory;

        public FileSystemService()
        {
            _filesDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Files");
        }


        public FileData SaveFile(HttpPostedFile postedFile)
        {
            if (!Directory.Exists(_filesDirectory))
                Directory.CreateDirectory(_filesDirectory);

            var fileid = Guid.NewGuid();

            var result = new FileData()
            {
                FileID = fileid,
                FileName = postedFile.FileName,                
            };

            var path = GetFilePath(result.FileID);

            var dir = Directory.GetParent(path).FullName;

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            else if (File.Exists(path))
                File.Delete(path);

            postedFile.SaveAs(path);

            return result;
        }


        public string GetFilePath(Guid fileid)
        {
            string guid = fileid.ToString("N");

            return Path.Combine(Path.Combine(_filesDirectory, guid.Substring(1, 4)), guid);
        }
    }
}
